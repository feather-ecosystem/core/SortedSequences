# SortedSequences

[![pipeline status](https://gitlab.com/feather-ecosystem/SortedSequences/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/SortedSequences/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/SortedSequences/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/SortedSequences/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/SortedSequences/branch/master/graph/badge.svg?token=7gEhrvPzk5)](https://codecov.io/gl/feather-ecosystem:core/SortedSequences)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/SortedSequences)
